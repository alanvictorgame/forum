﻿<?php
/** 
 * Classe d'accès aux données. 
 
 * Utilise les services de la classe PDO
 * pour l'application E-Massilia
 * Les attributs sont tous statiques,
 * les 4 premiers pour la connexion
 * $monPdo de type PDO 
 * $monPdoMassilia qui contiendra l'unique instance de la classe
 
 * @package default
 * @author Ludovic Genevois
 * @version    1.0
 */

class PdoMassilia{
      	private static $serveur='mysql:host=localhost';
      	private static $bdd='dbname=g4horizon';
      	private static $user='test';
      	private static $mdp='';
		private static $monPdo;
		private static $monPdoMassilia=null;
/**
 * Constructeur privé, crée l'instance de PDO qui sera sollicitée
 * pour toutes les méthodes de la classe
 */
	private function __construct(){
    	PdoMassilia::$monPdo = new PDO(PdoMassilia::$serveur.';'.PdoMassilia::$bdd, PdoMassilia::$user, PdoMassilia::$mdp);
		PdoMassilia::$monPdo->query("SET CHARACTER SET utf8");
	}
	public function _destruct(){
		PdoMassilia::$monPdo = null;
	}
/**
 * Fonction statique qui crée l'unique instance de la classe

 * Appel : $instancePdoMassilia = PdoMassilia::getPdoMassilia();

 * @return l'unique objet de la classe PdoMassilia
 */
	public  static function getPdoMassilia(){
		if(PdoMassilia::$monPdoMassilia==null){
			PdoMassilia::$monPdoMassilia= new PdoMassilia();
		}
		return PdoMassilia::$monPdoMassilia;
	}

/*
 * PARTIE A CODER POUR L'ACCES AUX DONNEES DE LA BASE
 *
 * EX: RECUPERER LA LISTE DES POSTES DU SITE INTERNET, METTRE A JOURS UN POSTE ... ETC
 *
 */

        public function getInfosUtilisateur($login, $mdp){
            $req = "SELECT administrateur.ID_ADMIN as id, administrateur.NOM_ADMIN as nom, administrateur.PRENOM_ADMIN as prenom, administrateur.ROLE_ADMIN as role FROM administrateur WHERE administrateur.LOGIN_ADMIN='$login' AND administrateur.MDP_ADMIN='$mdp'";
            $result = PdoMassilia::$monPdo->query($req);
            $ligne = $result->fetch();
            return $ligne;
        }
        public function getArticle($idpub)
        {
            $req="SELECT * FROM publication WHERE publication.ID_ARTICLE = '$idpub'";
            $result = PdoMassilia::$monPdo->query($req);
            $ligne = $result->fetch();
            return $ligne;
        }
        public function getReponses($idpub){
            $req = "SELECT * FROM reponse JOIN publication on publication.ID_ARTICLE = reponse.ID_ARTICLE JOIN administrateur ON administrateur.ID_ADMIN = reponse.ID_ADMIN  WHERE publication.ID_ARTICLE = '$idpub'";
            $result = PdoMassilia::$monPdo->query($req);
            $lesLignes = $result->fetchAll();
            return $lesLignes;
        }
    public function getArticles()
    {
        $req = "SELECT * FROM publication";
        $result = PdoMassilia::$monPdo->query($req);
        $lesLignes = $result->fetchAll();
        return $lesLignes;
    }
        public function getInfosUtilisateurs(){
            $req="SELECT administrateur.ID_ADMIN as id, administrateur.NOM_ADMIN as nom, administrateur.PRENOM_ADMIN as prenom, administrateur.ROLE_ADMIN as role FROM administrateur";
            $result = PdoMassilia::$monPdo->query($req);
            $lesLignes = $result->fetchAll();
            return $lesLignes;
        }
        public function deletUtilisateur($id){
            $req="DELETE FROM administrateur WHERE administrateur.ID_ADMIN='$id'";
            PdoMassilia::$monPdo->query($req);
        }
        public function deletPublication($id)
        {
            $req = "DELETE FROM reponse WHERE reponse.ID_ARTICLE='$id'";
            PdoMassilia::$monPdo->query($req);
            $req="DELETE FROM publication WHERE publication.ID_ARTICLE='$id'";
            PdoMassilia::$monPdo->query($req);
        }
        public function getLaPublication($id)
        {
            $req = "SELECT publication.ID_ARTICLE as ID, publication.TITRE_ARTICLE as titre, publication.CONTENU_ARTICLE as contenu, publication.DATE_PUB_ARTICLE as dateCreation, publication.DATE_MODIF_ARTICLE as dateModif  FROM publication WHERE publication.ID_ARTICLE=$id";
            $result = PdoMassilia::$monPdo->query($req);
            $ligne = $result->fetch();
            return $ligne;
        }

        public function getLesPublications(){
                $req = "SELECT publication.ID_ARTICLE as id, publication.TITRE_ARTICLE as titre, publication.CONTENU_ARTICLE as contenu, publication.DATE_PUB_ARTICLE as dateCreation, publication.DATE_MODIF_ARTICLE as dateModif FROM publication";
                $result = PdoMassilia::$monPdo->query($req);
                $lesLignes = $result->fetchAll();
                return $lesLignes;
        }

public function insertReponse($Contenu,$id, $idpub)
{
    $req = "INSERT INTO reponse(CONTENU_REP,ID_ADMIN, ID_ARTICLE) VALUES('$Contenu', '$id','$idpub');";
    $result = PdoMassilia::$monPdo->exec($req);
    if($result){
        return 1;
    }else{
        return 0;
    }
}
        public function insertPublication($titreFR, $texteFR)
        {
            $req = "INSERT INTO publication(TITRE_ARTICLE, CONTENU_ARTICLE, DATE_PUB_ARTICLE, DATE_MODIF_ARTICLE) VALUES('$titreFR', '$texteFR', now(),  now());";
            $result = PdoMassilia::$monPdo->exec($req);
            if($result){
                return 1;
            }else{
                return 0;
            }
        }

        public function updatePublication($id, $titreFR, $texteFR ){
            $req = "UPDATE publication SET TITRE_ARTICLE = '$titreFR', CONTENU_ARTICLE = '$texteFR', DATE_MODIF_ARTICLE= now() WHERE ID_ARTICLE = $id;";
            $result = PdoMassilia::$monPdo->exec($req);
            if($result){
                return 1;
            }else{
                return 0;
            }
        }

        public function getLesEmails()
        {
            $req = "SELECT ID_EMAIL, EMAIL_ADRESSE_EMAIL, EMAIL_TITRE_EMAIL, CONTENU_EMAIL, NOM, isEmailValide FROM EMAIL";
            $result = PdoMassilia::$monPdo->query($req);
            $lesLignes = $result->fetchAll();
            return $lesLignes;
        }

        public function putEmailValide($id)
        {
            $req = "UPDATE EMAIL SET isEmailValide = 1 WHERE ID_EMAIL = $id";
            $result = PdoMassilia::$monPdo->exec($req);
            if($result == 1){
            return 1;
            }else{
            return 0;
            }
        }
}
?>
