<?php
include("vues/v_sommaire.php");
$action = $_REQUEST['action'];
$idPub =null;
switch($action) {
    case 'rep': //amène vers le formulaire pour créer une nouvelle publication
    {
        $idPub = $_REQUEST['id_article'];
        $reponses = $pdo->getReponses($idPub);
        $article = $pdo->getArticle($idPub);
        include("vues/v_reponse.php");

        break;
    }
    case 'validation':
    {
        $Contenu = $_REQUEST['txtAreaFR'];
        $id = $_SESSION['idVisiteur'];
        $idPub = $_REQUEST['id_article'];
        $pdo = PdoMassilia::getPdoMassilia();
        if($pdo->insertReponse($Contenu,$id, $idPub ) == 1)
        {
            ajouterSucces("Création de la publication réalisée avec succes.");
            include("vues/v_succes.php");
            $article = $pdo->getArticle($idPub);
        $reponses = $pdo->getReponses($idPub);
            include("vues/v_reponse.php");
        }
        else
        {
            ajouterErreur("Une erreur est survenue dans la création de la publication.");
            include("vues/v_erreurs.php");
        }
        break;
    }
}
