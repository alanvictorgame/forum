<?php
include("vues/v_sommaire.php");
$action = $_REQUEST['action'];
switch($action){
    case 'ajouter': //amène vers le formulaire pour créer une nouvelle publication
	{
		include("vues/v_publication.php");
		break;
	}
	case 'validation':
	{
            $titreFR = $_POST['txtTitrePubFR'];
            $textFR = $_POST['txtAreaFR'];
            $pdo = PdoMassilia::getPdoMassilia();
            if($pdo->insertPublication($titreFR, $textFR ) == 1)
            {
                ajouterSucces("Création de la publication réalisée avec succes.");
                include("vues/v_succes.php");
            }
            else
            {
                ajouterErreur("Une erreur est survenue dans la création de la publication.");
                include("vues/v_erreurs.php");
            }
            include("vues/v_publication.php");
            break;
	}
}
?>
