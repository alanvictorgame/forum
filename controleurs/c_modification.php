<?php
include("vues/v_sommaire.php");
$action = $_REQUEST['action'];
$idVisiteur = $_SESSION['idVisiteur'];
$idPub = null;
switch($action){
	case 'modifier':{
            include("vues/v_listePublication.php");
            break;
	}
        case 'afficher':{
            $idPub = $_REQUEST['lstPublication'];
            $laPublication = $pdo->getLaPublication($idPub);
            $titre = $laPublication['titre'];
            $contenu = $laPublication['contenu'];
            $dateCreation = $laPublication['dateCreation'];
            include("vues/v_listePublication.php");
            include("vues/v_publicationAModifier.php");
            break;
        }
        case 'validation':
        {
            $idPublication = $_GET['idPublication'];
            $titreFRModifier = $_POST['txtTitrePubFR'];
            $texteFRModifier = $_POST['txtAreaModifFR'];
            if($pdo->updatePublication($idPublication, $titreFRModifier, $texteFRModifier) == 1)
            {
                ajouterSucces("Modification de la publication réalisée avec succes.");
                include("vues/v_succes.php");
            }
            else
            {
                ajouterErreur("Une erreur est survenue dans la modification de la publication.");
                include("vues/v_erreurs.php");
            }
            include("vues/v_listePublication.php");
            break;
        }
}
?>