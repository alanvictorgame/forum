<?php
require_once("include/fct.inc.php");
require_once ("include/pdomassilia.inc.php");
include("vues/v_entete.php") ;
session_start();
$pdo = PdoMassilia::getPdoMassilia();
$estConnecte = estConnecte();
if(!isset($_REQUEST['uc']) || !$estConnecte){
$_REQUEST['uc'] = 'connexion';
}
$uc = $_REQUEST['uc'];
switch($uc){
case 'connexion':{
include("controleurs/c_connexion.php");break;
}
case 'modification' :{
include("controleurs/c_modification.php");break;
}
case 'publication' :{
include("controleurs/c_publication.php");break;
}
case 'accueil' :{
include("controleurs/c_accueil.php");break;
}
    case 'administration':{
        include("controleurs/c_admin.php");break;
    }
    case 'suppression':{
        include("controleurs/c_suppression.php");break;
    }
    case 'reponse':{
        include("controleurs/c_reponse.php");break;
    }
}
include("vues/v_pied.php") ;
?>
