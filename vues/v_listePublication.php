<div id="contenu">
    <h2>Modification d'une publication</h2>
    <form action="index.php?uc=modification&action=afficher" method="post">
        <div class="corpsForm">
            <p>
                <label id="lblPubModif" for="lstPublication" accesskey="P"><strong>Choisir la publication à modifier :</strong></label>
                <select id="lstPublication" name="lstPublication">
                    <?php
                        $LesPublications = $pdo->getLesPublications();
                        foreach($LesPublications as $unePublication)
                        {
                            $id = $unePublication["id"];
                            echo '<option value='.$id.'>'.$unePublication['titre'].'</option>';
                        }
                    ?>
                </select> 
            </p>
        </div>
        <div class="piedForm">
            <p>
                <input id="ok" type="submit" value="Valider" size="20"  style="margin-right: 5px;"/>
            </p>

        </div>
    </form>
    