<div id="menuGauche">
    <div id="infoUtil">
        <h2>Tableau de bord</h2>
        <h3 class="null"><?php echo substr($_SESSION['nom'], 0, 1).".".$_SESSION['prenom'] ?></h3>
    </div>
    <ul id="menuList">
        <li class="smenu">
            <a class="null" href="index.php?uc=accueil&action=accueil" title="Allez à l'accueil">Accueil</a>
        </li>
        <li class="smenu">
            <a class="" href="index.php?uc=publication&action=ajouter" title="Ajouter une publication">Ajouter une publication</a>
        </li>
        <li class="smenu">
            <a class="null" href="index.php?uc=connexion&action=deconnexion" title="Se déconnecter">Deconnexion</a>
        </li>
        <?php if ($_SESSION["role"] == true){?>
        <li class="smenu">
            <a class="null" href="index.php?uc=modification&action=modifier" title="Modifier une publication">Modifier  une publication</a>
        </li>
            <li class="smenu">
                <a class="null" href="index.php?uc=suppression&action=supp" title="Modifier une publication">Supprimer  une publication</a>
            </li>
            <li class="smenu">
                <a class="null" href="index.php?uc=administration&action=admin" title="Vue Admin">Suppression utilisateur</a>
            </li>
        <?php }?>
    </ul>
</div>