<div id="contenu">
    <h2>Modification d'une publication</h2>
    <form action="index.php?uc=administration&action=supp" method="post">
        <div class="corpsForm">
            <p>
                <label id="lblUserSupp" for="lstUtilisateur" accesskey="P"><strong>Choisir un utilisateur a supprimé :</strong></label>
                <select id="lstUtilisateur" name="lstUtilisateur">
                    <?php
                    $Utilisateurs = $pdo->getInfosUtilisateurs();
                    foreach($Utilisateurs as $Utilisateur)
                    {
                        $id = $Utilisateur["id"];
                        echo '<option value='.$id.'>'.$Utilisateur['nom'].'</option>';
                    }
                    ?>
                </select>
            </p>
        </div>
        <div class="piedForm">
            <p>
                <input id="ok" type="submit" value="Valider" size="20"  style="margin-right: 5px;"/>
            </p>
        </div>
    </form>