﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
    <head>
        <title>G4HORIZON</title>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <link href="./styles/styles.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="./styles/font-awesome/css/font-awesome.min.css" />
        <link href="./styles/froala_editor.pkgd.min.css" rel="stylesheet" type="text/css" />
        <link href="./styles/froala_style.min.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="./scripts/jquery.min.js"></script>
        <script type="text/javascript" src="./scripts/froala_editor.pkgd.min.js"></script>
        <script type="text/javascript" src="./scripts/scripts.js"></script>
    </head>
    <body>
        <div id="page">
            <div id="entete">
                <h1>Bienvenue sur G4Horizon</h1>
            </div>

